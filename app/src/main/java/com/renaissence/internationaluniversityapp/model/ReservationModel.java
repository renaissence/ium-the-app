package com.renaissence.internationaluniversityapp.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Albano Vaz on 9/24/2016.
 */
@IgnoreExtraProperties
public class ReservationModel {



    private String uid;
    private String name;
    private String phoneNumber;
    private String date;
    private String time;
    public String endDate;
    public String endTime;
    private String duration;
    private String description;

    public ReservationModel() {
    }


    //public Map<String, Boolean> stars = new HashMap<>();

    public ReservationModel(String uid, String name, String phoneNumber, String date, String time,String endDate, String endTime, String duration) {
        this.uid = uid;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.date = date;
        this.time = time;
        this.endDate = endDate;
        this.endTime = endTime;
        this.duration = duration;

    }

    @Exclude
    public Map<String,Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", uid);
        result.put("name", name);
        result.put("phoneNumber", phoneNumber);
        result.put("date", date);
        result.put("time", time);
        result.put("endDate", endDate);
        result.put("endTime", endTime);
        result.put("duration", duration);
        return result;
    }
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDuration() {
        return duration;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
