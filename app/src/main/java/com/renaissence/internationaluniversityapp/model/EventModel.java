package com.renaissence.internationaluniversityapp.model;

import java.util.HashMap;
import java.util.Map;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Braulio on 11/30/2016.
 */

public class EventModel {

    public String id;
    public String batchCode;
    public String eventDate;
    public String eventTime;
    public String eventPeriod;
    public String eventTopic;
    public String eventRemarks;

    public EventModel (String id, String batchCode,String eventDate, String eventTime, String eventPeriod, String eventTopic, String eventRemarks){
        this.id = id;
        this.batchCode = batchCode;
        this.eventDate = eventDate;
        this.eventTime = eventTime;
        this.eventPeriod = eventPeriod;
        this.eventTopic = eventTopic;
        this.eventRemarks = eventRemarks;
    }

    public EventModel (String batchCode,String eventDate, String eventTime, String eventPeriod, String eventTopic, String eventRemarks){
        this.batchCode = batchCode;
        this.eventDate = eventDate;
        this.eventTime = eventTime;
        this.eventPeriod = eventPeriod;
        this.eventTopic = eventTopic;
        this.eventRemarks = eventRemarks;
    }
    public EventModel (){

    }

    @Exclude
    public Map<String,Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("BatchCode", batchCode);
        result.put("Date", eventDate);
        result.put("Time",eventTime);
        result.put("Period", eventPeriod);
        result.put("Topic", eventTopic);
        result.put("Remarks", eventRemarks);
        return result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public String getEventPeriod() {
        return eventPeriod;
    }

    public void setEventPeriod(String eventPeriod) {
        this.eventPeriod = eventPeriod;
    }

    public String getEventTopic() {
        return eventTopic;
    }

    public void setEventTopic(String eventTopic) {
        this.eventTopic = eventTopic;
    }

    public String getEventRemarks() {
        return eventRemarks;
    }

    public void setEventRemarks(String eventRemarks) {
        this.eventRemarks = eventRemarks;
    }
}
