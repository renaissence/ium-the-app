package com.renaissence.internationaluniversityapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;


public class ContactIumUActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_iumu);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contact_iumu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void openwebsite(View view) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.ium.edu.na")));
    }

    public void sendemail(View view) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "ium@ium.edu.na", null));
        //emailIntent.putExtra(Intent.EXTRA_SUBJECT, "EXTRA_SUBJECT");
        startActivity(emailIntent);
    }

    public void callphonenumber(View view) {
        //Uri number= "+264-433 6000";
        /*Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:+264-433 6000"));
        startActivity(intent);*/
        Intent emergencyIntent = new Intent(this, Contacts.class);
        startActivity(emergencyIntent);
    }
    public void emergencynumber(View view){
       // startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://renaissence.github.io/ium.github.io/")));
        Intent emergencyIntent = new Intent(this, EmergencyContacts.class);
        startActivity(emergencyIntent);
    }


}
