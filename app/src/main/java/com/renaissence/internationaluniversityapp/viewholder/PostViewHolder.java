package com.renaissence.internationaluniversityapp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.renaissence.internationaluniversityapp.R;
import com.renaissence.internationaluniversityapp.models.Post;

public class PostViewHolder extends RecyclerView.ViewHolder {

    public TextView titleView;
    public TextView authorView;
    public ImageView starView;
    public TextView numStarsView;
    public TextView bodyView;
    public TextView timeViewText;
    public TextView dateViewText;
    public TextView timeView;
    public TextView dateView;
    public TextView phoneNumber;
    private TextView mStatus;

    public PostViewHolder(View itemView) {
        super(itemView);

        titleView = (TextView) itemView.findViewById(R.id.post_title);
        authorView = (TextView) itemView.findViewById(R.id.post_author);
        starView = (ImageView) itemView.findViewById(R.id.star);
        numStarsView = (TextView) itemView.findViewById(R.id.post_num_stars);
        bodyView = (TextView) itemView.findViewById(R.id.post_body);
        timeView = (TextView) itemView.findViewById(R.id.post_time);
        dateView = (TextView) itemView.findViewById(R.id.post_date);
        timeViewText = (TextView) itemView.findViewById(R.id.post_time_text);
        dateViewText = (TextView) itemView.findViewById(R.id.post_date_text);
        phoneNumber = (TextView) itemView.findViewById(R.id.post_phonenumber);
        mStatus = (TextView) itemView.findViewById(R.id.status);

    }

    public void bindToPost(Post post, View.OnClickListener starClickListener) {
        String time = "Time:";
        String date = "Date:";
        String status = "Status: ";
        titleView.setText(post.title);
        authorView.setText(post.author);
        numStarsView.setText(String.valueOf(post.starCount));
        bodyView.setText(post.body);
        timeView.setText(post.time);
        dateView.setText(post.date);
        phoneNumber.setText(post.phoneNumber);
        timeViewText.setText(time);
        dateViewText.setText(date);
        mStatus.setText(status);

      //  starView.setOnClickListener(starClickListener);
    }
}
