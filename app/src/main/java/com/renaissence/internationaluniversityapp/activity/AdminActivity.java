package com.renaissence.internationaluniversityapp.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.renaissence.internationaluniversityapp.MainActivity;
import com.renaissence.internationaluniversityapp.R;
import com.renaissence.internationaluniversityapp.dataHelper.AdminViewHolder;
import com.renaissence.internationaluniversityapp.dataHelper.QueryHelper;
import com.renaissence.internationaluniversityapp.models.ReservationModel;

public class AdminActivity extends AppCompatActivity implements QueryHelper {

    RecyclerView recyclerView;
    String username;
    public static final String EXTRA_POST_KEY = "reservation_key";
    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    LinearLayoutManager mManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);


        recyclerView = (RecyclerView)findViewById(R.id.reservation_recycler_view);
        recyclerView.setHasFixedSize(true);
        mManager = new LinearLayoutManager(this);
        //mManager.setReverseLayout(true);
        //mManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(mManager);

        Query reservationQuery = getQuery(databaseReference);
        FirebaseRecyclerAdapter<ReservationModel,AdminViewHolder> mAdapter = new FirebaseRecyclerAdapter<ReservationModel, AdminViewHolder>(
                ReservationModel.class,
                R.layout.reservation,
                AdminViewHolder.class,
                reservationQuery
        ) {
            @Override
            protected void populateViewHolder(final AdminViewHolder viewHolder, final ReservationModel model, int position) {
                //String userID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                final DatabaseReference reservationReference = getRef(position);
                final String reservationKey = reservationReference.getKey();

                viewHolder.setTextName(model.getName());
                viewHolder.setTextTableNumber(model.getTableNumber());
                viewHolder.setTextNumSeats(model.getNumSeats());
                viewHolder.setTextDate(model.getDate());
                viewHolder.setTextTime(model.getTime());
                //viewHolder.setTextEndDate(model.getEndDate());
                viewHolder.setTextEndTime(model.getEndTime());

                viewHolder.sendMessage(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        new AlertDialog.Builder(AdminActivity.this).setIcon(android.R.drawable.stat_sys_warning).setTitle("Send Message")
                                .setMessage("")
                                .setIcon(R.drawable.images  )
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(Intent.ACTION_SEND);
                                        intent.setType("text/html");
                                        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"obama@gmail.com"});
                                        intent.putExtra(Intent.EXTRA_SUBJECT, "Reservation ");
                                        intent.putExtra(Intent.EXTRA_TEXT, "Good Day, "+ "Barack Obama " +" you have a reservation at Cassule's Restaurant.");
                                        try{
                                            startActivity(Intent.createChooser(intent, "Send Email"));
                                        }catch (android.content.ActivityNotFoundException ex){
                                            Toast.makeText(AdminActivity.this, "There are no email clients installed", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }).setNegativeButton("No", null).show();
                    }
                });

                viewHolder.sendEmail(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        new AlertDialog.Builder(AdminActivity.this).setIcon(android.R.drawable.stat_sys_warning).setTitle("Send Email")
                                .setMessage("")
                                .setIcon(R.drawable.download)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                }).setNegativeButton("No", null).show();
                    }
                });

            }
        };
        recyclerView.setAdapter(mAdapter);

    }
    /*
    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }
*/
    @Override
    public Query getQuery(DatabaseReference databaseReference) {


        return databaseReference.child("reservations").orderByChild("date");
    }

}
