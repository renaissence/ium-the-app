package com.renaissence.internationaluniversityapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.renaissence.internationaluniversityapp.MainActivity;
import com.renaissence.internationaluniversityapp.R;
import com.renaissence.internationaluniversityapp.dataHelper.NewViewHolder;
import com.renaissence.internationaluniversityapp.dataHelper.ReservationCardAdapter;
import com.renaissence.internationaluniversityapp.model.EventModel;

public class SaveReservationActivity extends ReservationActivity {

    private LayoutInflater inflater;
    private RecyclerView recycler;
    private ReservationCardAdapter adapter;

    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    //FirebaseRecyclerAdapter<EventModel, ViewHolder> mAdapter;
    LinearLayoutManager mManager;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_reservation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SaveReservationActivity.this, ReservationActivity.class);
                startActivity(intent);
            }
        });

        recycler = (RecyclerView) findViewById(R.id.recycler);
      /*  LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        recycler.setLayoutManager(layoutManager);
        adapter = new ReservationCardAdapter(this);
        recycler.setAdapter(adapter);*/

        recycler = (RecyclerView) findViewById(R.id.recycler);
        recycler.setHasFixedSize(true);
        mManager = new LinearLayoutManager(this);
        //mManager.setReverseLayout(true);
        //mManager.setStackFromEnd(true);
        recycler.setLayoutManager(mManager);

        // Read from the database

        databaseReference.addValueEventListener(new ValueEventListener() {
            String TAG ="";
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                Log.d(TAG, "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        Query eventQuery = getQuery(databaseReference);
        FirebaseRecyclerAdapter<EventModel, NewViewHolder> mAdapter = new FirebaseRecyclerAdapter<EventModel, NewViewHolder>(
                EventModel.class,
                R.layout.event_schedule_layout,
                NewViewHolder.class,
                eventQuery) {
            @Override
            protected void populateViewHolder(NewViewHolder viewHolder, final EventModel model, int position) {
                //String userID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                final DatabaseReference eventReference = getRef(position);
                final String eventKey = eventReference.getKey();

                //viewHolder.setIdText(.toString());
                //viewHolder.setBatchCode(model.getBatchCode());
                viewHolder.setEventDateText(model.getEventDate());
                viewHolder.setEventTImeText(model.getEventTime());
                //viewHolder.setEventPeriodText(model.getEventPeriod());
                //viewHolder.setEventTopic(model.getEventTopic());
                //viewHolder.setEventRemarkText(model.getEventRemarks());
            }
        };
        recycler.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_done) {

        }
        else if(id==R.id.action_logout){
            signOut();
        }
        return super.onOptionsItemSelected(item);
    }

    public Query getQuery(DatabaseReference databaseReference) {


        return databaseReference.child("eventClass");
    }
}
