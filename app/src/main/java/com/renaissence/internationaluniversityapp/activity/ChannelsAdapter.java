/*
 * $Id: $
 *
 * Copyright (C) 2016 Albano Vaz (albanov94@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

package com.renaissence.internationaluniversityapp.activity;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.util.LongSparseArray;
import android.support.v4.widget.CursorAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filterable;

import com.renaissence.internationaluniversityapp.content.ContentManager;
import com.renaissence.internationaluniversityapp.view.ChannelView;

import java.util.HashMap;

public class ChannelsAdapter extends CursorAdapter implements Filterable {

    private HashMap<Long, ChannelView> views;

    public ChannelsAdapter(Context context, Cursor c) {
        super(context, c, FLAG_REGISTER_CONTENT_OBSERVER);
        views = new HashMap<>();
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ((ChannelView) view).bindView(cursor);
        putView(cursor, (ChannelView) view);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        ChannelView view = new ChannelView(context, parent);
        view.bindView(cursor);
        putView(cursor, view);
        return view;
    }

    public ChannelView getView(long id) {
        return views.get(id);
    }

    private void putView(Cursor cursor, ChannelView view) {
        views.put(ContentManager.getChannelId(cursor), view);
    }
}