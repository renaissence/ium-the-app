package com.renaissence.internationaluniversityapp.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.renaissence.internationaluniversityapp.R;

public class BatchActivity extends AppCompatActivity {

    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_batch);
        //get firebase auth instance
        auth = FirebaseAuth.getInstance();

        //get current user
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    startActivity(new Intent(BatchActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };
        Button btnClasses = (Button) findViewById(R.id.btnEvents);
        Button btnUpdate = (Button) findViewById(R.id.btnUpdate);
        Button btnAddClass = (Button) findViewById(R.id.btnAddEvent);
        Button btnAddNewBatch = (Button) findViewById(R.id.butAddBatch);
        Button btnLogOut = (Button) findViewById(R.id.btnLogout);


    }
    public void listEvent(View view){
        Intent intent = new Intent(BatchActivity.this, SaveReservationActivity.class);
        startActivity(intent);
    }
    public void addBatch(View v){
        Intent intent = new Intent(BatchActivity.this, AddClassActivity.class);
        startActivity(intent);
    }
    //sign out method
    public void signOut(View view) {
        auth.signOut();
    }
    @Override
    protected void onResume() {
        super.onResume();
        //progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(authListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null) {
            auth.removeAuthStateListener(authListener);
        }
    }
}
