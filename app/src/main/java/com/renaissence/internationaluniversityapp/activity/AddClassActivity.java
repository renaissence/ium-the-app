package com.renaissence.internationaluniversityapp.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.renaissence.internationaluniversityapp.R;
import com.renaissence.internationaluniversityapp.model.EventModel;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class AddClassActivity extends AppCompatActivity {

    private static final String TAG = "EditReservationActivity";
    private static final int DATE_DIALOG = 1;
    private static final int TIME_DIALOG = 2;
    private int day, month, year, hours, mins;
    TextView textClassDate, textClassTime, textBatchCode;
    EditText editPeriod,editRemarks, editTopics;
    CheckBox chkAdjust;
    Button addEventBatch;
    private FirebaseUser user;
    private EventModel eventModel;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FirebaseAuth.getInstance().getCurrentUser();


        setContentView(R.layout.activity_add_class);
        textBatchCode = (TextView) this.findViewById(R.id.textBatchCode);
        textClassDate = (TextView) this.findViewById(R.id.textClassDate);
        textClassTime = (TextView) this.findViewById(R.id.textClassTime);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        editPeriod = (EditText) this.findViewById(R.id.editPeriod) ;
        editRemarks = (EditText) this.findViewById(R.id.editRemarks) ;
        editTopics = (EditText) this.findViewById(R.id.editTopics) ;

        chkAdjust = (CheckBox) this.findViewById(R.id.chkAdjust);
        textBatchCode.setText(   getIntent().getStringExtra("batchcode"));
        addEventBatch = (Button) this.findViewById(R.id.addEventBatch);
        addEventBatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createEvent();
            }
        });
        setDateToSysdate();
        updateDateDisplay();
    }

    public void createEvent() {

        final String batchCode = textBatchCode.getText().toString();
        final String eventDate = textClassDate.getText().toString();
        final String eventTime = textClassTime.getText().toString();
        final String eventPeriod = editPeriod.getText().toString();
        final String eventTopic = editTopics.getText().toString();
        final String eventRemarks = editRemarks.getText().toString();

        if (!TextUtils.isEmpty(eventPeriod) && !TextUtils.isEmpty(eventTopic)&& !TextUtils.isEmpty(eventRemarks)){
            Toast.makeText(getApplicationContext(), "Invalid Input!", Toast.LENGTH_SHORT).show();
        }
        final String userId = getUid();
        mDatabase.child("eventClass").child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                 eventModel = dataSnapshot.getValue(EventModel.class);

                if (user == null) {
                    // User is null, error out
                    Log.e(TAG, "User " + userId + " is unexpectedly null");
                    Toast.makeText(AddClassActivity.this,
                            "Error: could not fetch user.",
                            Toast.LENGTH_SHORT).show();
                } else {
                    saveEvent(userId,batchCode,eventDate, eventTime, eventPeriod, eventTopic, eventRemarks);
                    Handler myHandler = new Handler();
                    myHandler.postDelayed(mMyRunnable, 1000);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "GetUser: operation Has been Cancelled", databaseError.toException());
                // [START_EXCLUDE]
                //setEditingEnabled(true);

            }
        });


    }

    private void saveEvent(String id,String batchCode,String eventDate, String eventTime, String eventPeriod, String eventTopic, String eventRemarks){
        String key = mDatabase.child("eventClass").push().getKey();
        EventModel eventModel = new EventModel(id, batchCode,eventDate,eventTime,eventPeriod,eventTopic,eventRemarks);
        Map<String, Object> reservationValues = eventModel.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/eventClass/"+key, reservationValues);
        //childUpdates.put("/user-events/"+ id +"/"+key,reservationValues);
        mDatabase.updateChildren(childUpdates);
    }
    public String getUid() {


            return FirebaseAuth.getInstance().getCurrentUser().getUid();


    }

    private Runnable mMyRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            Intent intent = new Intent(AddClassActivity.this, SaveReservationActivity.class);
            startActivity(intent);

        }
    };
    private void setDateToSysdate() {
        Calendar c = Calendar.getInstance();
        day = c.get(Calendar.DAY_OF_MONTH);
        month = c.get(Calendar.MONTH);
        year = c.get(Calendar.YEAR);
    }

    public void showDatePicker(View v) {
        showDialog(DATE_DIALOG);
    }

    public void showTimePicker(View v) {
        showDialog(TIME_DIALOG);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        super.onCreateDialog(id);

        switch (id) {
            case DATE_DIALOG:
                return new DatePickerDialog(this, dateSetListener, year, month, day);
            case TIME_DIALOG:
                return new TimePickerDialog(this, timeSetListener, hours,mins, false);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int pYear, int pMonth, int pDay) {
            year = pYear;
            month = pMonth;
            day = pDay;
            updateDateDisplay();
        }
    };


    private TimePickerDialog.OnTimeSetListener timeSetListener =
            new TimePickerDialog.OnTimeSetListener() {

                @Override
                public void onTimeSet(TimePicker arg0, int pHours, int  pMins) {
                    hours = pHours;
                    mins = pMins;
                    updateTimeDisplay();
                }

            };



    private void updateDateDisplay() {
        // Month is 0 based so add 1
        textClassDate.setText(String.format("%04d-%02d-%02d", year, month + 1,day));
    }

    private void updateTimeDisplay() {
        // Month is 0 based so add 1
        textClassTime.setText(String.format("%02d:%02d", hours,mins));
    }
}