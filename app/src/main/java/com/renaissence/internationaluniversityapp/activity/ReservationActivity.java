package com.renaissence.internationaluniversityapp.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.renaissence.internationaluniversityapp.model.ReservationModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.renaissence.internationaluniversityapp.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

public class ReservationActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, DialogInterface.OnCancelListener, AdapterView.OnItemSelectedListener{

    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth mAuth;
    private Button mSubmitBtn;
    ReservationModel reservationModel;
    private FirebaseUser mCurrentUser;

    private DatabaseReference mDatabase;

    private DatabaseReference mDatabaseUser;

    public ProgressDialog mProgress;

    String tableNum;
    String date;
    String time;
    String seatNum;



    private int year, month, day, hour, minute;
    Spinner spinner;
    TextView tableNumber;
    TextView textViewTime;
    TextView textViewDate;
    EditText editTextName;
    EditText editTextPhone;
    Button buttonAddDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation);

        //get firebase mAuth instance
        mAuth = FirebaseAuth.getInstance();

        //get current mCurrentUser
         mCurrentUser = mAuth.getCurrentUser();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Events");

        mDatabaseUser = FirebaseDatabase.getInstance().getReference().child("Users").child(mCurrentUser.getUid());

        // Progress dialog
                mProgress = new ProgressDialog(this);

        mSubmitBtn = (Button) findViewById(R.id.eventSubmitBtn);


        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    // mCurrentUser mAuth state is changed - mCurrentUser is null
                    // launch login activity
                    startActivity(new Intent(ReservationActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };

        mSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        tableNumber = (TextView) findViewById(R.id.table_number);
        tableNum = getIntent().getStringExtra("MyData");
        tableNumber.setText(tableNum);
        editTextName = (EditText) findViewById(R.id.name);
        editTextPhone = (EditText) findViewById(R.id.phone_number);
        textViewDate = (TextView) findViewById(R.id.textview_date);
        textViewDate.setText(date);
        textViewTime = (TextView) findViewById(R.id.textview_time);
        textViewTime.setText(time);
        spinner = (Spinner) findViewById(R.id.spinner2);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(ReservationActivity.this,
                R.array.people, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(ReservationActivity.this);
        spinner.setAdapter(spinnerAdapter);
        buttonAddDate = (Button)findViewById(R.id.add_date);
        buttonAddDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scheduleReservation(view);
            }
        });
    }

    //sign out method
    public void signOut() {
        mAuth.signOut();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_done) {

            mProgress.setMessage("Submitting the Event Request...");
            mProgress.show();

            reservationModel = new ReservationModel();
              final String attendeeName = editTextName.getText().toString().trim();
            final String attendeePhone = editTextPhone.getText().toString().trim();

            if (textViewDate.getText() == null || textViewTime.getText() == null) {
                Toast.makeText(ReservationActivity.this, "Not saved, Please Enter Date & Time", Toast.LENGTH_SHORT).show();
            }
            else {

                final DatabaseReference newPost = mDatabase.push();

                mDatabaseUser.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        newPost.child("eventTitle").setValue(attendeeName);
                        newPost.child("name").setValue(attendeeName);
                        newPost.child("phoneNumber").setValue(attendeePhone);
                        newPost.child("uid").setValue(mCurrentUser.getUid());
                        newPost.child("username").setValue(dataSnapshot.child("name").getValue()).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                                if (task.isSuccessful()) {
                                    startActivity(new Intent(ReservationActivity.this, SaveReservationActivity.class));
                                } else {
                                    Toast.makeText(ReservationActivity.this, "Failed to post to the blog", Toast.LENGTH_SHORT).show();
                                }

                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {


                    }
                });

                mProgress.dismiss();
            }

            }else if (id==R.id.action_logout){
            signOut();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(authListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null) {
            mAuth.removeAuthStateListener(authListener);
        }
    }

    public void scheduleReservation(View view){
        initDateTimeData();
        Calendar cDefault = Calendar.getInstance();
        cDefault.set(year, month, day);

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                this,
                cDefault.get(Calendar.YEAR),
                cDefault.get(Calendar.MONTH),
                cDefault.get(Calendar.DAY_OF_MONTH)
        );

        Calendar cMin = Calendar.getInstance();
        Calendar cMax = Calendar.getInstance();
        cMax.set( cMax.get(Calendar.YEAR), 11, 31 );
        datePickerDialog.setMinDate(cMin);
        datePickerDialog.setMaxDate(cMax);

        List<Calendar> daysList = new LinkedList<>();
        Calendar[] daysArray;
        Calendar cAux = Calendar.getInstance();

        while( cAux.getTimeInMillis() <= cMax.getTimeInMillis() ){
            //you changed the day of the week to only sunday remember that ma nigga
            if( cAux.get( Calendar.DAY_OF_WEEK ) != Calendar.SUNDAY  ){
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis( cAux.getTimeInMillis() );

                daysList.add( c );
            }
            cAux.setTimeInMillis( cAux.getTimeInMillis() + ( 24 * 60 * 60 * 1000 ) );
        }
        daysArray = new Calendar[ daysList.size() ];
        for( int i = 0; i < daysArray.length; i++ ){
            daysArray[i] = daysList.get(i);
        }

        datePickerDialog.setSelectableDays( daysArray );
        datePickerDialog.setOnCancelListener(this);
        datePickerDialog.show( getFragmentManager(), "Date Picker" );
    }

    private void initDateTimeData(){
        if( year == 0 ){
            Calendar c = Calendar.getInstance();
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
            hour = c.get(Calendar.HOUR_OF_DAY);
            minute = c.get(Calendar.MINUTE);
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {

        year = month = day = hour = minute = 0;
        textViewDate.setText("");
        textViewTime.setText("");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int yearOfDacade, int monthOfYear, int dayOfMonth) {

        Calendar tDefault = Calendar.getInstance();
        tDefault.set(year, month, day, hour, minute);

        year = yearOfDacade;
        month = monthOfYear;
        day = dayOfMonth;

        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(
                this,
                tDefault.get(Calendar.HOUR_OF_DAY),
                tDefault.get(Calendar.MINUTE),
                true
        );
        timePickerDialog.setOnCancelListener(this);
        timePickerDialog.show(getFragmentManager(), "timePickerDialog");
        timePickerDialog.setTitle("Reservation Time");

        timePickerDialog.setThemeDark(true);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        seatNum = (adapterView.getItemAtPosition(position).toString());
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minuteOfHour, int second) {

        if( hourOfDay < 8 || hourOfDay > 21 ){
            onDateSet(null, year, month, day);
            Toast.makeText(this, "Only Between 8h and 22h", Toast.LENGTH_SHORT).show();
            return;
        }

        hour = hourOfDay;
        minute = minuteOfHour;

        date = ( (day < 10 ? "0"+day : day)+"/"+
                (month+1 < 10 ? "0"+(month+1) : month+1)+"/"+
                year);

        //resDate = textView.getText().toString();


        time = ((hour < 10 ? "0"+hour : hour)+"h"+
                (minute < 10 ? "0"+minute : minute));

        //resHour = textView2.getText().toString();
    }
}
