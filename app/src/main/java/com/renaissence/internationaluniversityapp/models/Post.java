package com.renaissence.internationaluniversityapp.models;

import com.google.android.gms.games.internal.api.StatsImpl;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

// [START post_class]
@IgnoreExtraProperties
public class Post {

    public String uid;
    public String author;
    public String title;
    public String body;
    public String time;
    public String date;
    public String phoneNumber;
    public String starCount = "";
    public Map<String, Boolean> stars = new HashMap<>();

    public Post() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public Post(String uid, String author, String title, String body, String time, String date, String phoneNumber) {
        this.uid = uid;
        this.author = author;
        this.title = title;
        this.body = body;
        this.time = time;
        this.date = date;
        this.phoneNumber = phoneNumber;
    }

    // [START post_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", uid);
        result.put("author", author);
        result.put("title", title);
        result.put("body", body);
        result.put("starCount", starCount);
        result.put("stars", stars);
        result.put("time", time);
        result.put("date", date);
        result.put("phoneNumber", phoneNumber);

        return result;
    }
    // [END post_to_map]

}
// [END post_class]
