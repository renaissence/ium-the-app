package com.renaissence.internationaluniversityapp.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Albano Vaz on 9/24/2016.
 */
@IgnoreExtraProperties
public class ReservationModel {

    private long id;
    private String name;
    private String phoneNumber;
    private String date;
    private String time;
    public String endDate;
    public String endTime;
    private String numSeats;
    private String duration;
    private String description;

    private String tableNumber;

    public ReservationModel() {
    }


    //public Map<String, Boolean> stars = new HashMap<>();

    public ReservationModel(String name, String phoneNumber, String tableNumber, String endDate, String endTime, String numSeats, String duration) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.tableNumber = tableNumber;
        this.date = date;
        this.time = time;
        this.endDate = endDate;
        this.endTime = endTime;
        this.numSeats = numSeats;
        this.duration = duration;
    }

    @Exclude
    public Map<String,Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("name", name);
        result.put("phoneNumber", phoneNumber);
        result.put("date", date);
        result.put("time", time);
        result.put("numberSeats", numSeats);
        result.put("duration", duration);
        result.put("endDate", endDate);
        result.put("endTime", endTime);
        return result;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNumSeats() {
        return numSeats;
    }

    public void setNumSeats(String numSeats) {
        this.numSeats = numSeats;
    }

    public String getDuration() {
        return duration;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(String tableNumber) {
        this.tableNumber = tableNumber;
    }
}
