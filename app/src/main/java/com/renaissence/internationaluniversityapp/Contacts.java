package com.renaissence.internationaluniversityapp;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

/**
 * Created by 305 on 11/28/2016.
 */

public class Contacts extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_contact);

        WebView myWebView = (WebView) findViewById(R.id.webview);
        myWebView.loadUrl("https://renaissence.github.io/ium.github.io/intro.html");
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }
}
