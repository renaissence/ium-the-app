package com.renaissence.internationaluniversityapp.dataHelper;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

/**
 * Created by Braulio on 10/8/2016.
 */

public interface QueryHelper {

    public abstract Query getQuery(DatabaseReference databaseReference);
}
