package com.renaissence.internationaluniversityapp.dataHelper;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.renaissence.internationaluniversityapp.R;

/**
 * Created by Albano Vaz on 10/7/2016.
 */

public class AdminViewHolder extends RecyclerView.ViewHolder {

    //private CardView card;
    LinearLayout layout;
    TextView textName;
    TextView textDate;
    TextView textTime;
    //TextView textEndDate;
    TextView textTableNumber;
    TextView textEndTime;
    TextView textNumSeats;
    Button sendEmail;
    Button sendSMS;
    View vv;
    View mView;

    public AdminViewHolder(View itemView) {
        // standard view holder pattern with Butterknife view injection
        super(itemView);

        mView = itemView;
       /*
        //card = (CardView) itemView.findViewById(R.id.card_books);
        textName = (TextView) itemView.findViewById(R.id.titleTextView);
        textNumSeats = (TextView)itemView.findViewById(R.id.text_view_seats);
        textDate = (TextView)itemView.findViewById(R.id.text_view_date);
        textTime = (TextView)itemView.findViewById(R.id.text_view_time);
        textEndDate = (TextView)itemView.findViewById(R.id.text_view_end_date);
        textEndTime = (TextView)itemView.findViewById(R.id.text_view_end_time);
*/      sendEmail = (Button)itemView.findViewById(R.id.btn_send_email);
        sendSMS = (Button)itemView.findViewById(R.id.btn_send_notification);
    }

    public void setTextName(String name) {
        textName = (TextView) mView.findViewById(R.id.titleTextView);
        textName.setText(name);
    }

    public void setTextNumSeats(String numSeats) {
        textNumSeats = (TextView)mView.findViewById(R.id.text_view_seat);
        textNumSeats.setText(numSeats);
    }

    public void setTextDate(String date) {
        textDate = (TextView)mView.findViewById(R.id.text_view_date);
        textDate.setText(date);
    }

    public void setTextTime(String time) {
        textTime = (TextView)mView.findViewById(R.id.text_view_time);
        textTime.setText(time);
    }
/*
    public void setTextEndDate(String endDate) {
        textEndDate = (TextView)mView.findViewById(R.id.text_view_end_date);
        textEndDate.setText(endDate);
    }
*/

    public void setTextEndTime(String endTime) {
        textEndTime = (TextView)mView.findViewById(R.id.text_view_end_time);
        textEndTime.setText(endTime);
    }

    public void setTextTableNumber(String tableNumber) {
        textTableNumber = (TextView) mView.findViewById(R.id.text_view_number_of_table);
        textTableNumber.setText(tableNumber);
    }
/*
    public void setLayout(){
        sendSMS = (Button)mView.findViewById(R.id.btn_send_notification);
        sendSMS = (Button)mView.findViewById(R.id.btn_send_email);
        vv = (View)mView.findViewById(R.id.view_view);
        layout = (LinearLayout)mView.findViewById(R.id.layout_id);
        layout.setBackgroundColor(Color.parseColor("#B71C1C"));
        editButton.setVisibility(View.VISIBLE);
        deleteButton.setVisibility(View.VISIBLE);
        vv.setVisibility(View.VISIBLE);
    }
*/
    public void sendMessage(View.OnClickListener sendMessaging){

        sendSMS.setOnClickListener(sendMessaging);
    }

    public void sendEmail(View.OnClickListener sendEmailing){

        sendEmail.setOnClickListener(sendEmailing);
    }
}