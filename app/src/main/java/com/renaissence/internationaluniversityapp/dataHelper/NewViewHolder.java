package com.renaissence.internationaluniversityapp.dataHelper;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.renaissence.internationaluniversityapp.R;

/**
 * Created by Albano Vaz on 12/1/2016.
 */

public class NewViewHolder extends RecyclerView.ViewHolder{

    public TextView eventDateText;
    public TextView eventTImeText;
    public TextView idText;
    View mView;

    public NewViewHolder(View itemView) {
        super(itemView);
        mView = itemView;
        eventDateText = (TextView) itemView.findViewById(R.id.tv_date_of_event);
        eventTImeText = (TextView) itemView.findViewById(R.id.tv_time_of_event);
        idText = (TextView)itemView.findViewById(R.id.text_view_id);
    }


    public void setEventDateText(String eventDate) {
        eventDateText = (TextView) mView.findViewById(R.id.tv_date_of_event);
        eventDateText.setText(eventDate);
    }

    public void setEventTImeText(String eventTIme) {
        eventTImeText = (TextView) mView.findViewById(R.id.tv_time_of_event);
        eventTImeText.setText(eventTIme);
    }

    public void setIdText(String id) {
        idText = (TextView) mView.findViewById(R.id.text_view_id);
        idText.setText(id);
    }
}
