package com.renaissence.internationaluniversityapp.dataHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces
 * <p/>
 * TO DO: Replace all uses of this class before publishing your app.
 */
public class SocialContent {

    public static List<SocialList> ITEMS = new ArrayList<>();

    public static Map<String, SocialList> ITEM_MAP = new HashMap<>();

    static {
        // Add gallery_item3 sample items.
        addItem(new SocialList("gallery_item1", "Facebook"));
        addItem(new SocialList("gallery_item2", "Twitter"));
        addItem(new SocialList("gallery_item3", "LinkedIn"));
        addItem(new SocialList("gallery_item4", "Google+"));
        addItem(new SocialList("gallery_item5", "Youtube"));
      //  addItem(new SocialList("6", "ChatRoom"));
        addItem(new SocialList("7", "Blackboard"));
    }

    private static void addItem(SocialList item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    public static class SocialList {
        public String id;
        public String content;

        public SocialList(String id, String content) {
            this.id = id;
            this.content = content;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
