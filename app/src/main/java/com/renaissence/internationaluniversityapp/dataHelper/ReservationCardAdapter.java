package com.renaissence.internationaluniversityapp.dataHelper;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.renaissence.internationaluniversityapp.R;
import com.renaissence.internationaluniversityapp.models.ReservationModel;
import com.bumptech.glide.Glide;

/**
 * Created by Albano Vaz on 9/24/2016.
 */
public class ReservationCardAdapter extends RecyclerView.Adapter<ReservationCardAdapter.ViewHolder>{

    final Context context;
    private LayoutInflater inflater;

    public ReservationCardAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reservation_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final ReservationModel reservationModel = new ReservationModel();

        holder.textName.setText(reservationModel.getName());
        holder.textPhone.setText(reservationModel.getPhoneNumber());
        holder.textTableNum.setText(reservationModel.getTableNumber());
        holder.textDate.setText(reservationModel.getDate());
        holder.textTime.setText(reservationModel.getTime());
        holder.textNumSeats.setText(reservationModel.getNumSeats());
        reservationModel.getDuration();
            Glide.with(context)
                    .load(reservationModel.getDuration().replace("https", "http"))
                    .asBitmap()
                    .fitCenter()
                    .into(holder.imageBackground);
    }

    @Override
    public int getItemCount() {
        return 0;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public CardView card;
        public TextView textName;
        public TextView textPhone;

        public TextView textTableNum;
        public TextView textDate;
        public TextView textTime;
        public TextView textNumSeats;

        public TextView textDescription;
        public ImageView imageBackground;

        public ViewHolder(View itemView) {
            // standard view holder pattern with Butterknife view injection
            super(itemView);

            card = (CardView) itemView.findViewById(R.id.card_books);
            textName = (TextView) itemView.findViewById(R.id.text_view_name);
            textPhone = (TextView) itemView.findViewById(R.id.text_view_phone_number);
            //textDescription = (TextView) itemView.findViewById(R.id.text_books_description);
            imageBackground = (ImageView) itemView.findViewById(R.id.image_background);
            textTableNum = (TextView)itemView.findViewById(R.id.text_view_table_number);
            textDate = (TextView)itemView.findViewById(R.id.text_view_date);
            textTime = (TextView)itemView.findViewById(R.id.text_view_time);
            textNumSeats = (TextView)itemView.findViewById(R.id.text_view_seats);
        }
    }
}
