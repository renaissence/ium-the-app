package com.renaissence.internationaluniversityapp;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Albano Vaz on 11/26/2016.
 */

        import android.os.Bundle;


        import com.renaissence.internationaluniversityapp.imageview.PhotoView;


        import android.app.Activity;
        import android.view.ViewGroup.LayoutParams;

public class IumGalleryActivity extends Activity {

    private ViewPager mViewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewPager = new HackyViewPager(this);
        setContentView(mViewPager);

        mViewPager.setAdapter(new SamplePagerAdapter());
    }

    static class SamplePagerAdapter extends PagerAdapter {

        private static int[] sDrawables = {
                R.drawable.gallery_item1, R.drawable.gallery_item2,
                R.drawable.gallery_item3, R.drawable.gallery_item4, R.drawable.gallery_item5
        };

        @Override
        public int getCount() {
            return sDrawables.length;
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {
            PhotoView photoView = new PhotoView(container.getContext());
            photoView.setImageResource(sDrawables[position]);

            // Now just add PhotoView to ViewPager and return it
            container.addView(photoView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            return photoView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }
}