/*
 * $Id: $
 *
 * Copyright (C) 2016 Albano Vaz (albanov94@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 */

package com.renaissence.internationaluniversityapp.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.renaissence.internationaluniversityapp.R;

public class DialogUtils {

    private DialogUtils() {
    }

    public static void showErrorDialog(Context ctx, String msg) {
        new AlertDialog.Builder(ctx)
            .setTitle(ctx.getResources().getText(R.string.error))
            .setMessage(msg)
            .setPositiveButton(ctx.getResources().getText(android.R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int wb) {
                    }
                }).create().show();
    }
}
