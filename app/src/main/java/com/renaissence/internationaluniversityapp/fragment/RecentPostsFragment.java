package com.renaissence.internationaluniversityapp.fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;


public class RecentPostsFragment extends PostListFragment {

    private FirebaseAuth mAuth;
    public RecentPostsFragment() {}

    @Override
    public Query getQuery(DatabaseReference databaseReference) {
        // [START recent_posts_query]
        // Last 100 posts, these are automatically the 100 most recent
        // due to sorting by push() keys
        // [END recent_posts_query]
      //  databaseReference= FirebaseDatabase.getInstance().getReference().child("posts");
//        String currentUserId = mAuth.getCurrentUser().getUid();
          //  Query query  =  databaseReference.orderByChild("uid").equalTo(currentUserId);

        //return query;
              return databaseReference.child("posts").limitToFirst(100);
    }
}
