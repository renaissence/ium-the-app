package com.renaissence.internationaluniversityapp.sked;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.renaissence.internationaluniversityapp.R;

public class HomeActivity extends AppCompatActivity {

    private DatabaseReference mDatase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        mDatase = FirebaseDatabase.getInstance().getReference().child("username");
 String userName = getIntent().getStringExtra("UserName");

        TextView textViewUserName = (TextView)findViewById(R.id.textViewUserName);
        textViewUserName.setText(userName);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


    }

}
/*
public class HomeActivity extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
*/